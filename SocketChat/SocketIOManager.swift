//
//  SocketIOManager.swift
//  SocketChat
//
//  Created by Maria Litvinenko on 31.01.2022.
//  Copyright © 2022 AppCoda. All rights reserved.
//

import UIKit
import SocketIO

class SocketIOManager: NSObject {
    
    static let sharedInstance = SocketIOManager()
    var manager: SocketManager = SocketManager(socketURL: NSURL(string: "http://192.168.0.100:3000")! as URL)
    var defaultNamespaceSocket : SocketIOClient!
    var socket : SocketIOClient!
    
    override init() {
        super.init()
        defaultNamespaceSocket = manager.defaultSocket
        socket = manager.socket(forNamespace: "/swift")
    }
    
    func establishConnection() {
        manager.connect()
    }
     
    func closeConnection() {
        manager.disconnect()
    }

    func connectToServerWithNickname(nickname: String, completionHandler: @escaping(_ userList: [[String: AnyObject]]?) -> Void) {
        socket.on("userList") { ( dataArray, ack) -> Void in
            completionHandler(dataArray[0] as? [[String: AnyObject]])
        }
    }
    
    func sendMessage(message: String, withNickname nickname: String) {
        socket.emit("chatMessage", nickname, message)
    }
    
    func exitChatWithNickname(nickname: String, completionHandler: () -> Void) {
        socket.emit("exitUser", nickname)
        completionHandler()
    }
    
    func getChatMessage(completionHandler: @escaping(_ messageInfo: [String: AnyObject]) -> Void) {
        socket.on("newChatMessage") { (dataArray, socketAck) -> Void in
            var messageDictionary = [String: AnyObject]()
            messageDictionary["nickname"] = dataArray[0] as AnyObject
            messageDictionary["message"] = dataArray[1] as AnyObject
            messageDictionary["date"] = dataArray[2] as AnyObject
            completionHandler(messageDictionary)
        }
    }

}
